//
//  Corelib.m
//  Corelib
//
//  Created by Антон Кудряшов on 11.12.15.
//  Copyright © 2015 Антон Кудряшов. All rights reserved.
//

#import "Corelib.h"

#import "CoreItem.h"

@implementation Corelib

+ (CoreItem *)createItem
{
    return [CoreItem new];
}

@end
