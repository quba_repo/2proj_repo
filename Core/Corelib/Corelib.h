//
//  Corelib.h
//  Corelib
//
//  Created by Антон Кудряшов on 11.12.15.
//  Copyright © 2015 Антон Кудряшов. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CoreItem;

@interface Corelib : NSObject

+ (CoreItem*)createItem;

@end
