//
//  XibSegue.m
//  baby.ru
//
//  Created by Антон Кудряшов on 08.07.15.
//  Copyright (c) 2015 Improve Digital. All rights reserved.
//

#import "PushXibSegue.h"

@implementation PushXibSegue

- (instancetype)initWithIdentifier:(NSString *)identifier
                            source:(UIViewController *)source
                       destination:(UIViewController *)destination
{
    return [super initWithIdentifier:identifier source:source destination:[[destination class] new]];
}

- (void)perform
{
    UIViewController *source = (UIViewController *)self.sourceViewController;
    [source.navigationController pushViewController:self.destinationViewController animated:YES];
}

@end
