//
//  main.m
//  gg2
//
//  Created by Антон Кудряшов on 10.12.15.
//  Copyright © 2015 Антон Кудряшов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
