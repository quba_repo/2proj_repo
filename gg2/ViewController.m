//
//  ViewController.m
//  gg2
//
//  Created by Антон Кудряшов on 10.12.15.
//  Copyright © 2015 Антон Кудряшов. All rights reserved.
//

#import "ViewController.h"

#import "Corelib/Corelib.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    id obj = [Corelib createItem];
    NSLog(@"copied %@", obj);
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pushXIB:(id)sender
{
    [self performSegueWithIdentifier:@"XIB" sender:nil];
}

@end
