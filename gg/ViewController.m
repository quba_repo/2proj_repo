//
//  ViewController.m
//  gg
//
//  Created by Антон Кудряшов on 10.12.15.
//  Copyright © 2015 Антон Кудряшов. All rights reserved.
//

#import "ViewController.h"

#import "Corelib/Corelib.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    id item = [Corelib createItem];
    NSLog(@"Yes item too here %@" , item);
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
